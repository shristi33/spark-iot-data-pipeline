-- Create table in HIVE
CREATE TABLE TEMP(id bigint, id_mesh varchar(255), id_wasp varchar(255), id_secret varchar(255), sensor varchar(255),value double, datetime timestamp) STORED AS ORC;
-- Map HIVE table to DynamoDB
CREATE EXTERNAL TABLE TEMPTODYNAMO(id bigint, id_mesh string, id_wasp string, id_secret string, sensor string,value double, datetime string) STORED BY 'org.apache.hadoop.hive.dynamodb.DynamoDBStorageHandler' TBLPROPERTIES ("dynamodb.table.name" = "TestDB","dynamodb.column.mapping" = "id:id,id_mesh:id_mesh,id_wasp:id_wasp,id_secret:id_secret,sensor:sensor,value:value,datetime:datetime");
-- Load the temporary table data to DynamoDB
INSERT OVERWRITE TABLE TEMPTODYNAMO SELECT * FROM TEMP;