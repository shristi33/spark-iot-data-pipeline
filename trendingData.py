from pyspark.sql import SparkSession    # for spark session
from decimal import Decimal     # DynamoDB only support Decimal format not float
import boto3    # to write data into DynamoDB
import time     # to check the data loading and processing time
init_time = time.time();    # store initial time
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('trendingData')  # DynamoDB table
year = '2019'
# months = ['05','06','07','08']
months = ['0'+str(i) if len(str(i)) == 1 else str(i) for i in range(5,9)]
# S3 file path for all the 'months' 
fileNames = ["s3://iotlatechbucket/" + year + "/" + i + "/*/*/*" for i in months]
# creating a spark session for the trendingData job
spark = SparkSession\
    .builder\
    .appName("trendingData")\
    .getOrCreate()
# initialize for storing all the dataframe
union_df = None
# for loop to union all the 'months' dataframe
for i in fileNames:
    fileTime = time.time()
    df = spark.read.format("s3selectCSV").load(i, inferSchema='true').withColumnRenamed('_c0','id').withColumnRenamed('_c1','id_mesh').withColumnRenamed('_c2','id_wasp').withColumnRenamed('_c3','id_secret').withColumnRenamed('_c4','sensor').withColumnRenamed('_c5','value').withColumnRenamed('_c6','datetime')
    if union_df is None:
        union_df = df
    else:
        union_df = union_df.union(df)
    print("Time taken to load file for Month: " + i + " =" + str(time.time() - fileTime))
print("Total data count: " + str(union_df.count()))
print("Total data loading time: " + str(time.time() - init_time))
data_processing_init_time = time.time()
# convert dataframe into rdd for data processing
Rdd = union_df.rdd
# group rdd by sensor node
group_rdd = Rdd.groupBy(lambda x: x.id_wasp)
totalNodes = group_rdd.count()
data = {}
# iterate through the grouped data to accumulate the data by date and sensor node
for (k,v) in group_rdd.take(totalNodes):
    for i in v:
        # extract date from in YYYY-mm-dd format from datetime
        date_id =  i.datetime.strftime("%Y-%m-%d")
        # create key e.g., node_SW#2019-08-11
        key = str(k + '#' + str(date_id))
        if key not in data:
            # initialize data for each key
            data[key] = {
                'id': key,
                'trendingData':{
                    'sum':{},
                    'count':{},
                    'avg':{},
                    'max':{},
                    'min':{}
                }
            }
        # Calculate stats (sum, count, avg, max, min) for the sensor values
        data[key]['trendingData']['sum'][i.sensor] = (Decimal(str(i.value))  +  (Decimal(str(data[key]['trendingData']['sum'][i.sensor]))) if i.sensor in data[key]['trendingData']['sum'] else Decimal(str(0.0)))
        data[key]['trendingData']['count'][i.sensor] = (1)  + (data[key]['trendingData']['count'][i.sensor] if i.sensor in data[key]['trendingData']['count'] else (0))
        data[key]['trendingData']['avg'][i.sensor] = Decimal(str(data[key]['trendingData']['sum'][i.sensor]/data[key]['trendingData']['count'][i.sensor]))
        data[key]['trendingData']['max'][i.sensor] = Decimal(str(i.value)) if i.sensor not in data[key]['trendingData']['max'] else max(Decimal(str(i.value)), Decimal(str(data[key]['trendingData']['max'][i.sensor])))
        data[key]['trendingData']['min'][i.sensor] = Decimal(str(i.value)) if i.sensor not in data[key]['trendingData']['min'] else min(Decimal(str(i.value)), Decimal(str(data[key]['trendingData']['min'][i.sensor])))
# iterate through data to import data into trendingData table in DynamoDB
with table.batch_writer() as batch:  
    for (k1,v1) in data.items():
        batch.put_item(Item={
            'id': k1,
            'sensor_node': k1.split('#')[0],
            'datetime': k1.split('#')[1],
            'trendingData': v1['trendingData']
        })
print("Data processing time: " + str(time.time() - data_processing_init_time))
print("Total Spark job time: " + str(time.time() - init_time))